#!/bin/sh

set -eu

# Load environment variable from /etc/iocage-env
. load_env

# Generate some configuration from templates.
sync_configuration

# Generate self-signed TLS certificates
generate_self_signed_tls_certificates

# Enable the necessary services
sysrc -f /etc/rc.conf nginx_enable="YES"
sysrc -f /etc/rc.conf redis_enable="YES"

# Start the service
service nginx start 2>/dev/null
service redis start 2>/dev/null

# Make the default log directory
mkdir /var/log/zm
chown www:www /var/log/zm

# create sessions tmp dir outside nextcloud installation
mkdir -p /usr/local/www >/dev/null 2>/dev/null
chmod o-rwx /usr/local/www
chown -R www:www /usr/local/wwww

# Removing rwx permission on the nextcloud folder to others users
chmod -R o-rwx /usr/local/www
# Give full ownership of the nextcloud directory to www
chown -R www:www /usr/local/www

#echo "Database Name: $DB" > /root/PLUGIN_INFO
#echo "Database User: $USER" >> /root/PLUGIN_INFO
#echo "Database Password: $PASS" >> /root/PLUGIN_INFO

#echo "Nextcloud Admin User: $NCUSER" >> /root/PLUGIN_INFO
#echo "Nextcloud Admin Password: $NCPASS" >> /root/PLUGIN_INFO
